# Pixyz Loader For Unity

Import .pxz file via C# API (at runtime or authoring time)



[[_TOC_]]





# Description

Pixyz Loader for Unity is a Unity package containing an API capable of importing a **.pxz** file into a Unity scene at runtime.

This product can also be used to create prefabs programmatically in Unity Editor from a .pxz file.

You will need a Pixyz Studio or a Pixyz Scenario Processor product to convert/optimize your original 3D/CAD/PC files into .pxz files, before importing them into Unity.





## Features

![img/feature.jpg](media/features.jpg)

## Process

![img/process.jpg](media/process.jpg)





# Limitations



For now, Pixyz Loader for Unity supports **Windows** and **Debian** OS



# Licensing

By using this product you agree to respect Pixyz [General terms and conditions](https://www.pixyz-software.com/legal/general-user-and-licensing-terms-and-conditions/).

This product is delivered as a pkg for Unity and can be used and redistributed **free of charge**.

A licence is delivered with the product. As Pixyz format may evolve with future main releases, you may need to update your Pixyz Loader also to guarantee full compatibility between all Pixyz Products.

You can freely embed the Pixyz Loader in your Unity application, provided that you respect the [General terms and conditions](https://www.pixyz-software.com/legal/general-user-and-licensing-terms-and-conditions/) which are the same for all our products.

Your end-users won't need to do any specific enrolling towards the Unity or Pixyz website to use the functionality in your product.







# Setup



1. At authoring time, import Pixyz Loader Unity package in your Unity project (package accessible from [pxz-loader-package](https://gitlab.com/pixyz/samples/plugin-unity/pxzloaderforunity/-/tree/master/pxz-loader-package) folder)

   

2. Before running Pixyz Loader for Unity on a PC for the first time, a setup command has to be triggered. 

   *Please note that this command is automatically triggered on Windows when first instantiating the Pixyz Loader Importer object. But it's not automatic on Linux for user rights reasons, so you will need to launch this command*:

   - Linux: `sudo ./yourUnityProjectPath/Assets/Plugins/PixyzLoader/Runtime/Bin/PiXYZFinishInstall PiXYZLoader4Unity`
   - Windows:  `yourUnityProjectPath\Assets\Plugins\PixyzLoader\Runtime\Bin\PiXYZFinishInstall.exe PiXYZLoader4Unity`

   

4. Instantiate and run `Pixyz.Loader.Runtime.Importer` object

   ```C#
   var importer = new Pixyz.Loader.Runtime.Importer("filePath");
   importer.run();
   ```

   







# Sample code



## Runtime sample



**[pxzLoaderScript.cs](https://gitlab.com/pixyz/samples/plugin-unity/pxzloaderforunity/-/blob/master/samples/runtime-import-script/pxzLoaderScript.cs)**

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixyz.Loader.Runtime;

public class pxzLoaderScript : MonoBehaviour
{
    public string filePath;
    
    private void Start()
    {
        import();
    }
    
    void import()
    {
        string fullPath = Application.dataPath + "/" + filePath;
        Importer importer = null;
        try
        {
            importer = new Importer(fullPath);
        }
        catch (Pixyz.NoValidLicenseException)
        {
        }

        importer.progressed += onProgressChanged;
        importer.completed += onImportEnded;
        importer.run();
    }
    
    void onImportEnded(GameObject gameObject)
    {
        Debug.Log("Model Imported");
    }
    
    void onProgressChanged(float progress, string message)
    {
        Debug.Log("Progress : " + 100f * progress + "%");
    }

}


```







## Create prefab programmatically from a .pxz file

Add both C# files in your Unity project.



[**GenericImporter.cs**](https://gitlab.com/pixyz/samples/plugin-unity/pxzloaderforunity/-/blob/master/samples/create-prefab-from-pxz/GenericImporter.cs)

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Pixyz.Commons.Extensions;


public class GenericImporter
{

    private static string prefabPath;

    public static void ImportFile(string filePath, string prefabFilePath)
    {

        Debug.Log("Importing: " + filePath);
        
        prefabPath = prefabFilePath;

        try
        {
            var importer = new Pixyz.Loader.Runtime.Importer(filePath);
            importer.isAsynchronous = false;
            importer.completed += onImportEnded;
            importer.run();
    
        }
        catch (Pixyz.NoValidLicenseException)
        {   
        }
    }
    
    static void onImportEnded(GameObject gameObject)
    {
      
        Debug.Log("Model Imported");
        
        //create prefab
        gameObject.CreatePrefab(prefabPath);

    }
    
    static void onProgressChanged(float progress, string message)
    {
        Debug.Log("Progress : " + 100f * progress + "%");
    }

}
```



[**EditorExtensions.cs**](https://gitlab.com/pixyz/samples/plugin-unity/pxzloaderforunity/-/blob/master/samples/create-prefab-from-pxz/EditorExtensions.cs)

```c#
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Pixyz.Commons.Extensions
{
    public static partial class EditorExtensions
    {
        // Start is called before the first frame update
        public static GameObject CreatePrefab(this GameObject gameObject, string path)
        {
            IList<UnityEngine.Object> dependencies = GetNonPersistentDependenciesRecursive(gameObject);

            path = path + ".prefab";
            /// Ensures directory exists (recursive)
            string projectPath = Directory.GetParent(Application.dataPath).FullName;
            Directory.CreateDirectory(Path.GetDirectoryName(projectPath + "/" + path));
    
            /// We create prefab
            GameObject prefab = PrefabUtility.SaveAsPrefabAsset(gameObject, path);
            /// We add the dependencies
            if (dependencies != null)
            {
                foreach (var dependency in dependencies)
                {
                    if (dependency != null)
                    {
                        AssetDatabase.AddObjectToAsset(dependency, prefab);
                    }
                }
            }
    
            prefab = PrefabUtility.SaveAsPrefabAssetAndConnect(gameObject, path, InteractionMode.AutomatedAction);
            AssetDatabase.SaveAssets();
    
            return prefab;
        }
    
        public static UnityEngine.Object[] GetNonPersistentDependenciesRecursive(this GameObject gameObject)
        {
            var gameObjects = gameObject.GetChildren(true, true);
            var dependencies = new HashSet<UnityEngine.Object>();
    
            foreach (var child in gameObjects)
            {
                GetNonPersistentDependencies(child, ref dependencies);
            }
    
            return dependencies.ToArray();
        }
    
        public static void GetNonPersistentDependencies(this GameObject gameObject, ref HashSet<UnityEngine.Object> dependencies)
        {
            var components = gameObject.GetComponents<Component>();
            foreach (var component in components)
            {
                SerializedObject objSO = new SerializedObject(component);
                SerializedProperty property = objSO.GetIterator();
    
                do
                {
                    if (property.propertyType != SerializedPropertyType.ObjectReference
                        || !property.objectReferenceValue)
                        continue;
    
                    switch (property.objectReferenceValue)
                    {
                        case UnityEngine.Material material:
                            Shader shader = material.shader;
                            for (int i = 0; i < ShaderUtil.GetPropertyCount(shader); i++)
                            {
                                if (ShaderUtil.GetPropertyType(shader, i) == ShaderUtil.ShaderPropertyType.TexEnv)
                                {
                                    Texture texture = material.GetTexture(ShaderUtil.GetPropertyName(shader, i));
                                    if (texture && !AssetDatabase.Contains(texture))
                                    {
                                        dependencies.Add(texture);
                                    }
                                }
                            }
                            if (!AssetDatabase.Contains(material))
                            {
                                dependencies.Add(material);
                            }
                            break;
                        case Mesh mesh:
                            if (!AssetDatabase.Contains(mesh))
                            {
                                dependencies.Add(mesh);
                            }
                            break;
                        default:
                            break;
                    }
                } while (property.Next(true));
            }
        }
    }

}
```

# Know issues

- Animations contained in .pxz files  are not imported in Unity when Pixyz Loader is used.

- Surface patches present in a .pxz file can have a significant rendering performance impact in your Unity scene. We advice you to delete patches from your 3D model in your Pixyz Studio or Scenario Processor.

  

# More information about Pixyz

- [Website](https://www.pixyz-software.com/)
- [Support](https://www.pixyz-software.com/support/)

