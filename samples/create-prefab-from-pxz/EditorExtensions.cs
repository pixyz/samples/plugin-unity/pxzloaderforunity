﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Pixyz.Commons.Extensions
{
    public static partial class EditorExtensions
    {
        // Start is called before the first frame update
        public static GameObject CreatePrefab(this GameObject gameObject, string path)
        {
            IList<UnityEngine.Object> dependencies = GetNonPersistentDependenciesRecursive(gameObject);

            path = path + ".prefab";
            /// Ensures directory exists (recursive)
            string projectPath = Directory.GetParent(Application.dataPath).FullName;
            Directory.CreateDirectory(Path.GetDirectoryName(projectPath + "/" + path));

            /// We create prefab
            GameObject prefab = PrefabUtility.SaveAsPrefabAsset(gameObject, path);
            /// We add the dependencies
            if (dependencies != null)
            {
                foreach (var dependency in dependencies)
                {
                    if (dependency != null)
                    {
                        AssetDatabase.AddObjectToAsset(dependency, prefab);
                    }
                }
            }

            prefab = PrefabUtility.SaveAsPrefabAssetAndConnect(gameObject, path, InteractionMode.AutomatedAction);
            AssetDatabase.SaveAssets();

            return prefab;
        }

        public static UnityEngine.Object[] GetNonPersistentDependenciesRecursive(this GameObject gameObject)
        {
            var gameObjects = gameObject.GetChildren(true, true);
            var dependencies = new HashSet<UnityEngine.Object>();

            foreach (var child in gameObjects)
            {
                GetNonPersistentDependencies(child, ref dependencies);
            }

            return dependencies.ToArray();
        }

        public static void GetNonPersistentDependencies(this GameObject gameObject, ref HashSet<UnityEngine.Object> dependencies)
        {
            var components = gameObject.GetComponents<Component>();
            foreach (var component in components)
            {
                SerializedObject objSO = new SerializedObject(component);
                SerializedProperty property = objSO.GetIterator();

                do
                {
                    if (property.propertyType != SerializedPropertyType.ObjectReference
                        || !property.objectReferenceValue)
                        continue;

                    switch (property.objectReferenceValue)
                    {
                        case UnityEngine.Material material:
                            Shader shader = material.shader;
                            for (int i = 0; i < ShaderUtil.GetPropertyCount(shader); i++)
                            {
                                if (ShaderUtil.GetPropertyType(shader, i) == ShaderUtil.ShaderPropertyType.TexEnv)
                                {
                                    Texture texture = material.GetTexture(ShaderUtil.GetPropertyName(shader, i));
                                    if (texture && !AssetDatabase.Contains(texture))
                                    {
                                        dependencies.Add(texture);
                                    }
                                }
                            }
                            if (!AssetDatabase.Contains(material))
                            {
                                dependencies.Add(material);
                            }
                            break;
                        case Mesh mesh:
                            if (!AssetDatabase.Contains(mesh))
                            {
                                dependencies.Add(mesh);
                            }
                            break;
                        default:
                            break;
                    }
                } while (property.Next(true));
            }
        }
    }

}
