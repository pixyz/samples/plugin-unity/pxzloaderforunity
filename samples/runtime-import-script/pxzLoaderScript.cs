﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixyz.Loader.Runtime;

public class pxzLoaderScript : MonoBehaviour
{
    public string filePath;

    private void Start()
    {
        import();
    }

    void import()
    {
        string fullPath = Application.dataPath + "/" + filePath;
        Importer importer = null;
        try
        {
            importer = new Importer(fullPath);
        }
        catch (Pixyz.NoValidLicenseException)
        {
        }

        importer.progressed += onProgressChanged;
        importer.completed += onImportEnded;
        importer.run();
    }

    void onImportEnded(GameObject gameObject)
    {
        Debug.Log("Model Imported");
    }

    void onProgressChanged(float progress, string message)
    {
        Debug.Log("Progress : " + 100f * progress + "%");
    }

}